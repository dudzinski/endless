﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Endless;

public class CameraController : MonoBehaviour {

    public Transform player;
    public Vector3 offset;
    public float maxRotation;
    public float speedRotation;

    public bool follow;
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (follow)
        {   
            transform.position = player.position + offset;

            float direction = (float)InputManager.GetDirection();
            transform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(transform.localEulerAngles.z, maxRotation * -direction, speedRotation * Time.deltaTime));
        }
    }
}
