﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Endless
{
    public static class InputManager
    {

        public enum Direction {left = -1, nothing = 0, right = 1};
        public static Direction _Direction;

        public static Direction GetDirection()
        {
            int Dir = 0;

            if (Input.touchCount == 0)
            {
                if (Input.GetKey(KeyCode.A) || (Input.GetMouseButton(0) && Input.mousePosition.x <= Screen.width / 2))
                {
                    Dir--;
                }

                if (Input.GetKey(KeyCode.D) || (Input.GetMouseButton(0) && Input.mousePosition.x > Screen.width / 2))
                {
                    Dir++;
                }
            }
            else
            {
                if (Input.GetTouch(0).position.x <= Screen.width / 2)
                {
                    Dir--;
                }

                if (Input.GetTouch(0).position.x > Screen.width / 2)
                {
                    Dir++;
                }

                /*if (Input.touchCount >= 1)
                {
                    if (Input.GetTouch(1).position.x <= Screen.width / 2 && Dir != -1)
                    {
                        Dir--;
                    }

                    if (Input.GetTouch(1).position.x > Screen.width / 2 && Dir != 1)
                    {
                        Dir++;
                    }
                }
                */
            }
            return (Direction)Dir;
        }
    }
}

