﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTile : MonoBehaviour {

    private Transform palyer;

    private void Start()
    {

        palyer = GameObject.FindGameObjectWithTag("Player").transform;
    }
    // Update is called once per frame
    void Update ()
    {
        if (palyer.position.z > transform.position.z + 20)
        {
            Destroy(gameObject);
        }
	}
}
